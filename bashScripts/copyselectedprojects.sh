#!/bin/bash
base_dir="/home/saini/data/advancement/"
dest_folder=$base_dir"selected_projects/"
extracted_folder=$base_dir"extracted_source/"
rm -rf $dest_folder
mkdir -p $dest_folder
filename="/home/saini/data/advancement/output/projects_top100.txt"
echo "copying"
for project in `cat $filename`
do
    echo "cp -r $extracted_folder$project $dest_folder"
    cp -r $extracted_folder$project $dest_folder
done
echo "done!"
