#!/bin/bash
base_dir="/home/saini/code/ast/output_ast/"
dataset_folder=$base_dir"dataset/"
for project in `ls $dataset_folder`
do
    num_methods=$(cat $dataset_folder$project | wc -l)
    echo "$project, $num_methods"
done
