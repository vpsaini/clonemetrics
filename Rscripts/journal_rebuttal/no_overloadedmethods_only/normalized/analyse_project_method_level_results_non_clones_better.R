source("Rscripts/dataservice.R")
library(tools)

BASE_FOLDER <- "/media/saini/a0a5f724-b889-4788-8b3e-fce8d3884653/advancement/"
OUTPUT_FOLDER <- paste(BASE_FOLDER,"output/journal/",sep="")
stats_path <- paste(OUTPUT_FOLDER,"statistical_tests/no_overloaded_methods_only/",sep="")
input_folder <- paste(stats_path,"normalized/per_project_method_level_equal_num_methods_clones/",sep="")
total_projects <- 3562

header<- paste("Metric",
               "num_filter_pvalue_only",
               "num_filter_pvalue__percent",
               "r_0_1","r_0_1_percent",
               "r_1_3","r_1_3_percent",
               "r_3_5","r_3_5_percent",
               "r_5_10","r_5_10_percent",
               sep=",")
#par(mfrow=c(3,3))
destFileName <- paste(stats_path,"effect_size_with_eq_methods_onesided_non_clones_better_atleast_small_effect.csv",sep="") 
write(header,file=destFileName,append=F)
file_suffix = "*_onesided_nc_less_comlex.csv"

for(filename in list.files(path = input_folder, pattern = file_suffix, 
                           all.files = FALSE,
                           full.names = TRUE, recursive = TRUE,
                           include.dirs = FALSE)){
  print(paste("processing",filename,sep=" "))
  data <- read.csv(filename,header = TRUE,
                   stringsAsFactors=FALSE,
                   strip.white=TRUE,
  )
  drops <- c('sd_c','sd_nc')
  data <- data[,!(names(data) %in% drops)]
  data <- data[complete.cases(data),]
  metric <- unlist(strsplit(basename(file_path_sans_ext(filename)),split='_', fixed=T))[1] # metric name
  data <- subset(data,P.Value< 0.05)
  data$r <- abs(data$effect.size)
  data <- subset(data,r> 0.1)
  num_filter_pvalue_only <- nrow(data)
  num_filter_pvalue__percent<-round((num_filter_pvalue_only*100)/total_projects,1)
 
  
  #h = hist(data$r, main=metric)
  # h$density = h$counts/sum(h$counts)*100
  # plot(h, freq=F, ylab='Percentage',main=metric, xlab="Effect-size") 
  
  
  r_0_1 <- nrow(subset(data,r>=0 & r < 0.1))
  r_0_1_p <- round((r_0_1*100)/num_filter_pvalue_only,1)
  r_1_3 <- nrow(subset(data,r>=0.1 & r< 0.3))
  r_1_3_p <- round((r_1_3*100)/num_filter_pvalue_only,1)
  r_3_5 <- nrow(subset(data,r>=0.3 & r< 0.5))
  r_3_5_p <- round((r_3_5*100)/num_filter_pvalue_only,1)
  r_5_10 <- nrow(subset(data,r>=0.5))
  r_5_10_p <- round((r_5_10*100)/num_filter_pvalue_only,1)
  row <- paste(metric,
               num_filter_pvalue_only, num_filter_pvalue__percent,
               r_0_1,r_0_1_p,
               r_1_3,r_1_3_p,
               r_3_5,r_3_5_p,
               r_5_10,r_5_10_p,
               sep=",")
  write(row,file=destFileName,append=TRUE)
}
#dev.off()