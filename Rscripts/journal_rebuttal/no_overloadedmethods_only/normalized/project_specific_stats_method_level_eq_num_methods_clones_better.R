source("Rscripts/dataservice.R")
library(tools)

BASE_FOLDER <- "/media/saini/a0a5f724-b889-4788-8b3e-fce8d3884653/advancement/"
OUTPUT_FOLDER <- paste(BASE_FOLDER,"output/journal/",sep="")
FILE_SUFFIX = "*.csv"
folder<-"project_specific_clone_metric_10_10_no_overloaded_methods_only_new/"
input_folder <- paste(OUTPUT_FOLDER,folder,sep="") # yes this is the input folder, it is using files created in the output folder :D
output_file_suffix <- "_onesided_c_less_comlex.csv"


#Hodges-Lehmann estimator as effect size
#(http://stats.stackexchange.com/questions/30455/how-to-determine-the-effect-size-of-a-wilcoxon-rank-sum-test-in-r)

destFolder <- paste(OUTPUT_FOLDER,"statistical_tests/no_overloaded_methods_only/normalized/per_project_method_level_equal_num_methods_clones/",sep="")

dir.create(destFolder, showWarnings = TRUE, recursive = T, mode = "0777")
header<- paste("System",
               "P-Value",
               "effect-size",
               "Z-Value",
               "V or W",
               "alternative",
               "summary_statistic",
               "median_c",
               "median_nc", 
               "mean_c",
               "mean_nc",
               "sd_c",
               "sd_nc",
               "num_clones",
               "num_nonclones",
               sep="," )
metrics<- c("COMP", "NOCL", "NOS", "HLTH", "HVOC", 
            "HEFF","HBUG","CREF","XMET","LMET",
            "NLOC","NOC","NOA","MOD","HDIF",
            "VDEC","EXCT","EXCR","CAST","TDN",
            "HVOL","NAND","VREF","NOPR","MDN",
            "NEXP","LOOP"
)
dir.create(destFolder,recursive = TRUE)

# uncomment this if running first time, comment this to run in recovery mode
for(metric in metrics){
  destFileName <- paste(destFolder,metric,output_file_suffix,sep="")
  write(header,file=destFileName,append=F)
}

rerun<-3
processed_projects<- paste(destFolder,"XMET",output_file_suffix,sep="")
processed_projects_frame <- read.csv(processed_projects,header = TRUE,
                                     stringsAsFactors=FALSE,
                                     strip.white=TRUE,
)

for(filename in list.files(path = input_folder, pattern = FILE_SUFFIX, 
                           all.files = FALSE,
                           full.names = TRUE, recursive = TRUE,
                           include.dirs = FALSE)){
  
  tryCatch({
    
    data <- read.csv(filename,header = TRUE,
                     stringsAsFactors=FALSE,
                     strip.white=TRUE,
    )
    
    project_name <- basename(file_path_sans_ext(filename))
    
    if(project_name %in% processed_projects_frame$System){
      print(paste("ignoring",project_name,sep=":")) 
    }else{
      print(paste("processing file",filename,sep=":"))
      data_c <- subset(data,hasClone==1)
      data_nc <- subset(data,hasClone==0)
      num_clones <- nrow(data_c)
      num_nonclones <- nrow(data_nc)
      
      
      for(metric in metrics){
        # make sure clones and non clones have equal methods
        
        zvector <- vector()
        rvector <- vector()
        
        test_statistic_vector <- vector()
        
        median_metric_clones <- vector()
        median_metric_nonclones <- vector()
        
        mean_metric_clones <- vector()
        mean_metric_nonclones <- vector()
        
        sd_metric_clones <- vector()
        sd_metric_nonclones <- vector()
        
        pvector <- vector()
        count<-1
        for(i in 1:rerun){
          
          
          if(num_clones>num_nonclones){
            data_clones <- data_c[sample(1:num_clones,num_nonclones, replace = F),]
            data_nonclones <- data_nc
          } else if (num_clones<num_nonclones){
            data_nonclones <- data_nc[sample(1:num_nonclones,num_clones, replace = F),]
            data_clones <- data_c
          }else{
            data_clones <- data_c
            data_nonclones <- data_nc
          }
          
          
          tryCatch({
            wtest<- wilcox.test(x=data_clones[[metric]]/data_clones$NOS,y=data_nonclones[[metric]]/data_nonclones$NOS,paired = F,conf.int = T,alternative="less")
            N <- nrow(data_clones) + nrow(data_nonclones) # sum of observations in groupA and groupB 
            # get z and r (effect size) 
            
            z_and_r <- rFromWilcox(wtest,N)
            zvector[count] <- z_and_r[1]
            rvector[count] <- z_and_r[2]
            pvector[count] <- wtest$p.value
            test_statistic_vector[count] <- wtest$statistic
            median_metric_clones[count] <- median(data_clones[[metric]]/data_clones$NOS)
            median_metric_nonclones[count] <- median(data_nonclones[[metric]]/data_nonclones$NOS)
            mean_metric_clones[count] <- mean(data_clones[[metric]]/data_clones$NOS)
            mean_metric_nonclones[count] <- mean(data_nonclones[[metric]]/data_nonclones$NOS)
            sd_metric_clones[count] <- sd(data_clones[[metric]]/data_clones$NOS)
            sd_metric_nonclones[count] <- sd(data_nonclones[[metric]]/data_nonclones$NOS)
            count <- count + 1
            
          }, error = function (e){
            print (paste("error caught: metric", metric, ", system:", project_name, "err_msg: ", e, sep=" " ))
          })
        }
        # create a row to write in the file
        tryCatch({
          row <- paste(project_name, 
                       median(pvector),
                       median(rvector), 
                       median(zvector), 
                       median(test_statistic_vector),
                       wtest$alternative,
                       "point",
                       median(median_metric_clones),
                       median(median_metric_nonclones),
                       mean(mean_metric_clones),
                       mean(mean_metric_nonclones),
                       sd(sd_metric_clones),
                       sd(sd_metric_nonclones),
                       nrow(data_clones),
                       nrow(data_nonclones),
                       sep=",")
          # write to file
          destFileName <- paste(destFolder,metric,output_file_suffix,sep="")
          write(row,file=destFileName,append=TRUE)
        }, error = function (e){
          print (paste("error caught: metric", metric, ", system:", project_name, "err_msg: ", e, sep=" " ))
        })
      }
    }
  }, error = function (e){
    print (paste("error caught: file", filename, "err_msg: ", e, sep=" " ))
  })
  
}
print("done!")
