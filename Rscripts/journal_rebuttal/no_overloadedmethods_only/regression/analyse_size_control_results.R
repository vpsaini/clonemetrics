source("Rscripts/dataservice.R")
library(tools)


total_projects <- 3562

BASE_FOLDER <- "/media/saini/a0a5f724-b889-4788-8b3e-fce8d3884653/advancement/"
OUTPUT_FOLDER <- paste(BASE_FOLDER,"output/journal/",sep="")
stats_path <- paste(OUTPUT_FOLDER,"statistical_tests/no_overloaded_methods_only/",sep="")
sourceFolder <- paste(stats_path,"regression/metric_wise/",sep="")
destFolder <- paste(stats_path,"regression/summary/",sep="")
dir.create(destFolder, showWarnings = TRUE, recursive = T, mode = "0777")

header<- paste("Metric_names","Metric",
               "num_significant_projects_with_effect_size_at_least_small",
               "percent_significant_projects_with_effect_size_at_least_small",
               "num_projects_clones_better",
               "percent_projects_clones_better",
               "c_g_0_1","c_g_0_1_percent",
               "c_g_1_3","c_g_1_3_percent",
               "c_g_3_5","c_g_3_5_percent",
               "c_g_5_10","c_g_5_10_percent",
               "c_g_1_10", "c_g_1_10_percent",
               "num_projects_nonclones_better",
               "percent_projects_nonclones_better",
               "nc_g_0_1","nc_g_0_1_percent",
               "nc_g_1_3","nc_g_1_3_percent",
               "nc_g_3_5","nc_g_3_5_percent",
               "nc_g_5_10","nc_g_5_10_percent",
               "nc_g_1_10", "nc_g_1_10_percent",
               sep=",")

destFileName <- paste(destFolder,"regression_summary.csv",sep="") 
write(header,file=destFileName,append=F)

prepareRowData <- function(data,num_significant_projects){
  r_0_1 <- nrow(subset(data,abs_hedges_g>=0 & abs_hedges_g < 0.1))
  r_0_1_p <- round((r_0_1*100)/num_significant_projects,1)
  r_1_3 <- nrow(subset(data,abs_hedges_g>=0.1 & abs_hedges_g< 0.3))
  r_1_3_p <- round((r_1_3*100)/num_significant_projects,1)
  r_3_5 <- nrow(subset(data,abs_hedges_g>=0.3 & abs_hedges_g< 0.5))
  r_3_5_p <- round((r_3_5*100)/num_significant_projects,1)
  r_5_10 <- nrow(subset(data,abs_hedges_g>=0.5))
  r_5_10_p <- round((r_5_10*100)/num_significant_projects,1)
  r_1_10 <-  nrow(subset(data,abs_hedges_g>=0.1))
  r_1_10_p <- round((r_1_10*100)/num_significant_projects,1)
  
  row <- paste(r_0_1,r_0_1_p,
               r_1_3,r_1_3_p,
               r_3_5,r_3_5_p,
               r_5_10,r_5_10_p,
               r_1_10,r_1_10_p,
               sep=",")
  return (row)
}


for(filename in list.files(path = sourceFolder, pattern = FILE_SUFFIX, 
                           all.files = FALSE,
                           full.names = TRUE, recursive = TRUE,
                           include.dirs = FALSE)){
  print(paste("processing",filename,sep=" "))
  data <- read.csv(filename,header = TRUE,
                   stringsAsFactors=FALSE,
                   strip.white=TRUE,
  )
  data <- data[complete.cases(data),]
  metric <- basename(file_path_sans_ext(filename)) # metric name
  data <- subset(data,p_value_hasClone< 0.05) # select only significant projects
  data <- subset(data,R_Sq> .65) # select only projects where model explained more than 65% of the varaince in the metric
  data <- subset(data,abs_hedges_g>=.1)
  num_significant_projects_with_effect_size_at_least_small <- nrow(data)
  percent_significant_projects_with_effect_size_at_least_small<-round((num_significant_projects_with_effect_size_at_least_small*100)/total_projects,1)
  
  data_clones_better <- subset(data, slope_hasclone<0)
  num_projects_clones_better <- nrow(data_clones_better)
  num_projects_clones_better_percent <-round((num_projects_clones_better*100)/num_significant_projects_with_effect_size_at_least_small,1)
  
  data_nonclones_better <-subset(data, slope_hasclone>0)
  num_projects_nonclones_better <- nrow(data_nonclones_better)
  num_projects_nonclones_better_percent <-round((num_projects_nonclones_better*100)/num_significant_projects_with_effect_size_at_least_small,1)
  
  row <- paste(metric, paste(metric,"(",num_significant_projects_with_effect_size_at_least_small,")",sep=""),
               num_significant_projects_with_effect_size_at_least_small,percent_significant_projects_with_effect_size_at_least_small, 
               num_projects_clones_better,num_projects_clones_better_percent,
               prepareRowData(data_clones_better,num_significant_projects_with_effect_size_at_least_small),
               num_projects_nonclones_better,num_projects_nonclones_better_percent,
               prepareRowData(data_nonclones_better,num_significant_projects_with_effect_size_at_least_small),
               sep=",")

  write(row,file=destFileName,append=TRUE)
}
data_summary <- read.csv(destFileName,header = TRUE,
                 stringsAsFactors=FALSE,
                 strip.white=TRUE,
)
data_summary<- data_summary[!grepl("NOS", data_summary$Metric),] # remove the NOS entry
#data_ggplot <- data.frame()
#row <- c("Metric","Percent","Method_Type")
#data_ggplot <- rbind(data_ggplot,row)
metric <- vector()
percent <- vector()
method_type <- vector()

metric_names <- c(data_summary$Metric_names, data_summary$Metric_names)
metric <- c(data_summary$Metric,data_summary$Metric)
percent <- c(data_summary$percent_projects_clones_better,data_summary$percent_projects_nonclones_better)
method_type <- c(rep("Clones",nrow(data_summary)),rep("Non-Clones",nrow(data_summary)))

data_ggplot <- data.frame("Metric_names"=metric_names,"Metric"= metric, "Projects" = percent, "Method_type" = method_type)
data_ggplot <- data_ggplot[order(data_ggplot$Metric_names),]
data_documentation <- data_ggplot[data_ggplot$Metric_names %in% METRICS_DOCUMENTATION,]
data_modularity <- data_ggplot[data_ggplot$Metric_names %in% METRICS_MODULARITY,]
data_complexity <- data_ggplot[data_ggplot$Metric_names %in% METRICS_COMP,]
library(ggplot2)
folderpath <- "/home/saini/Dropbox/vaibhav-hitesh/advancement-clonemetric/plots/jouranl-rebuttal/no_overloaded_methods_only/regression/"
dir.create(folderpath, showWarnings = TRUE, recursive = T, mode = "0777")
pdf(paste(folderpath,"documentation-regression-size-controlled_rplot.pdf",sep=""),5,1.5) # width, height, in inches
par(mar=c(4,4.5,0.5,1)) # b,l,t,r

ggplot(data=data_documentation, aes(x= Metric, y= Projects, fill=Method_type)) +
  scale_fill_grey(start=.4)+
  geom_bar( stat="identity") + coord_flip() +
    guides(fill=guide_legend(title="Method Type"))+
  theme(text = element_text(size = 12,colour = "BLACK"), 
        axis.text = element_text(size = 10),
        legend.text = element_text(size=9),
        legend.title = element_text(size=9, colour="BLACK"))
dev.off()
pdf(paste(folderpath,"modularity_regression_size_ctrl_rplot.pdf",sep=""),5,2) # width, height, in inches
ggplot(data=data_modularity, aes(x= Metric, y=Projects, fill=Method_type)) +
  scale_fill_grey(start=.4)+
  geom_bar( stat="identity") + coord_flip() +
  guides(fill=guide_legend(title="Method Type"))+
  theme(text = element_text(size = 12,colour = "BLACK"), 
        axis.text = element_text(size = 10),
        legend.text = element_text(size=9),
        legend.title = element_text(size=9, colour="BLACK"))
dev.off()
pdf(paste(folderpath,"complexity_regression_size_ctrl_rplot.pdf",sep=""),7,5) # width, height, in inches
ggplot(data=data_complexity, aes(x= Metric, y=Projects, fill=Method_type)) +
  scale_fill_grey(start=.4)+
  geom_bar( stat="identity") + coord_flip() +
  guides(fill=guide_legend(title="Method Type"))+
  theme(text = element_text(size = 15,colour = "BLACK"), 
        axis.text = element_text(size = 13),
        legend.text = element_text(size=14),
        legend.title = element_text(size=14, colour="BLACK"))
  dev.off()