source("Rscripts/dataservice.R")
library(tools)
stats_path <- "/home/saini/data/advancement/output/statistical_tests/"
non_clones_filename <- paste(stats_path,"effect_size_no_size_control_onesided_nonclones_better_atleast_small_effect.csv",sep="") 
clones_filename <- paste(stats_path,"effect_size_no_size_control_onesided_clones_better_atleast_small_effect.csv",sep="") 


data_c <- read.csv(clones_filename,header = TRUE,
                   stringsAsFactors=FALSE,
                   strip.white=TRUE,
)
data_nc <- read.csv(non_clones_filename,header = TRUE,
                    stringsAsFactors=FALSE,
                    strip.white=TRUE,
)


data_c <- data_c[order(data_c$Metric),]
data_nc <- data_nc[order(data_nc$Metric),]


data_c<- data_c[!grepl("NOS", data_c$Metric),] # remove the NOS entry
data_nc<- data_nc[!grepl("NOS", data_nc$Metric),] # remove the NOS entry


significant_projects <- c(data_c$num_filter_pvalue_only + data_nc$num_filter_pvalue_only,data_c$num_filter_pvalue_only + data_nc$num_filter_pvalue_only)

metric_names <- c(data_nc$Metric, data_nc$Metric) # using data_nc , as both nc and c has same metrics
metric <- c(paste(metric_names,"(",significant_projects,")",sep=""))
percent <- c(data_c$num_filter_pvalue_only,data_nc$num_filter_pvalue_only)
percent <- round(percent*100/significant_projects,1)
method_type <- c(rep("Clones",nrow(data_c)),rep("Non-Clones",nrow(data_nc)))


data_ggplot <- data.frame("Metric_names"=metric_names,"Metric"= metric, "Projects" = percent, "Method_type" = method_type)
data_ggplot <- data_ggplot[order(data_ggplot$Metric_names),]
data_documentation <- data_ggplot[data_ggplot$Metric_names %in% METRICS_DOCUMENTATION,]
data_modularity <- data_ggplot[data_ggplot$Metric_names %in% METRICS_MODULARITY,]
data_complexity <- data_ggplot[data_ggplot$Metric_names %in% METRICS_COMP,]
library(ggplot2)

pdf('/home/saini/Dropbox/vaibhav-hitesh/advancement-clonemetric/plots/no-size-ctrl/documentation_no_size_ctrl_rplot.pdf',5,1.5) # width, height, in inches
par(mar=c(4,4.5,0.5,1)) # b,l,t,r
ggplot(data=data_documentation, aes(x= Metric, y= Projects, fill=Method_type)) +
  geom_bar( stat="identity") + coord_flip() +
  guides(fill=guide_legend(title="Method Type"))#+
  #theme(text = element_text(size = 15,colour = "BLACK",face = "bold"), axis.text = element_text(size = 15,colour = "BLACK",face = "bold"))
dev.off()
pdf('/home/saini/Dropbox/vaibhav-hitesh/advancement-clonemetric/plots/no-size-ctrl/modularity_no_size_ctrl_rplot.pdf',5,2) # width, height, in inches
ggplot(data=data_modularity, aes(x= Metric, y=Projects, fill=Method_type)) +
  geom_bar( stat="identity") + coord_flip() +
  guides(fill=guide_legend(title="Method Type"))#+
  #theme(text = element_text(size = 15,colour = "BLACK",face = "bold"), axis.text = element_text(size = 15,colour = "BLACK",face = "bold"))
dev.off()

pdf('/home/saini/Dropbox/vaibhav-hitesh/advancement-clonemetric/plots/no-size-ctrl/complexity_no_size_ctrl_rplot.pdf',7,5) # width, height, in inches
ggplot(data=data_complexity, aes(x= Metric, y=Projects, fill=Method_type)) +
  geom_bar( stat="identity") + coord_flip() +
  guides(fill=guide_legend(title="Method Type"))
#+theme(text = element_text(size = 15,colour = "BLACK",face = "bold"), axis.text = element_text(size = 15,colour = "GREY",face = "bold"))
dev.off()