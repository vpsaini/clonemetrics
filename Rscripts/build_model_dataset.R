source("Rscripts/dataservice.R")
library(tools)
stats_path <- "/home/saini/data/advancement/output/statistical_tests/"
size_control <- F
if(size_control){
  folder <- paste(stats_path,"per_project_method_level_equal_num_methods/",sep="")
}else{
  folder <- paste(stats_path,"per_project_method_level/",sep="")
}

if(size_control){
  destFileName <- paste(stats_path,"dataset_no_size_control.csv") 
}else{
  destFileName <- paste(stats_path,"dataset_with_size_control.csv") 
}

consolidated_frame_1 <- data.frame()
consolidated_frame_2 <- data.frame()
for(filename in list.files(path = folder, pattern = FILE_SUFFIX, 
                           all.files = FALSE,
                           full.names = TRUE, recursive = TRUE,
                           include.dirs = FALSE)){
  
  data <- read.csv(filename,header = TRUE,
                   stringsAsFactors=FALSE,
                   strip.white=TRUE,
  )
  data <- data[complete.cases(data),]
  data <- subset(data,P.Value< 0.05)
  data$r <- abs(data$effect.size)
  data_1 <- subset(data,r>=0.1)
  consolidated_frame_1 <- rbind(consolidated_frame_1,data.frame(System=data_1$System))
  data_2 <- subset(data,r>=0.3)
  consolidated_frame_2 <- rbind(consolidated_frame_2,data.frame(System=data_2$System))
}
consolidated_frame_1<- data.frame(System=unique(as.character(consolidated_frame_1$System)))
consolidated_frame_2<- data.frame(System=unique(as.character(consolidated_frame_2$System)))
clone_metric_filename <- paste(OUTPUT_FOLDER,"consolidated/","merged_clone_metrics_min_non_clones_10_min_clones_10.csv",sep="")
data <- read.csv(clone_metric_filename,header = TRUE,
                 stringsAsFactors=FALSE,
                 strip.white=TRUE,
)
metric_clone_1 = merge(x = data, y = consolidated_frame_1, by="System")
metric_clone_2 = merge(x = data, y = consolidated_frame_2, by="System")
tryCatch({
  destFileName <- paste(stats_path,"dataset_r_0.1.csv",sep="")
  write.csv(metric_clone_1,destFileName,row.names=FALSE)
  destFileName <- paste(stats_path,"dataset_r_0.3.csv",sep="")
  write.csv(metric_clone_2,destFileName,row.names=FALSE)
}, error = function (e){
  print (paste("error caught writing consolidated frame ", destFileName, " err_msg: ", e, sep=" " ))
})

