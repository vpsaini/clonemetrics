source("Rscripts/dataservice.R")
library(tools)
folder<-"project_specific_clone_metric_10_10/"
input_folder <- paste(OUTPUT_FOLDER,folder,sep="") # yes this is the input folder, it is using files created in the output folder :D


# uncomment this if running first time, comment this to run in recovery mode
#pdf('/home/saini/Dropbox/vaibhav-hitesh/advancement-clonemetric/plots/norm-regress-diff/with_without_norm_plots.pdf',14,12)
#Hodges-Lehmann estimator as effect size
par(mfrow =c(5,2))
metrics<- c("COMP", "HBUG","MDN","NOCL", "HLTH", "HVOC", 
            "HEFF","CREF","XMET","LMET",
            "NLOC","NOC","NOA","MOD","HDIF",
            "VDEC","EXCT","EXCR","CAST","TDN",
            "HVOL","NAND","VREF","NOPR",
            "NEXP","LOOP"
)

n= 5

par(mar=c(5,6,0,1)) # b,l,t,r
  for(filename in list.files(path = input_folder, pattern = FILE_SUFFIX, 
                             all.files = FALSE,
                             full.names = TRUE, recursive = TRUE,
                             include.dirs = FALSE)){
    
    for (metric in metrics){    
      data <- read.csv(filename,header = TRUE,
                       stringsAsFactors=FALSE,
                       strip.white=TRUE,
      )
      lab = 1
      project_name <- basename(file_path_sans_ext(filename))
      frm <- as.formula(paste(metric,"~ NOS",sep=""))
      mod1 <- lm(frm, data=data)
      corr1 = cor(x=data$NOS, y=data[[metric]],method = "spearman")  
     
      frm2 <- as.formula(paste(metric,"/NOS ~ NOS",sep=""))
      mod2 <- lm(frm2, data=data)
      corr2 = cor(x=data$NOS, y=data[[metric]]/data$NOS,method = "spearman")
     
     
#      frm3 <- as.formula(paste("log(",metric,") ~ log(NOS)",sep=""))
#      mod3 <- lm(frm3, data=data)
#      #plot(x=log(data$NOS) , y= log(data[[metric]]), main = " ", ylab = paste("log(",metric,")",sep=""),xlab = "log(NOS)",cex.lab=lab)
#      # axis(cex.axis=1.2)
#      #abline(mod3)
#      
#      
#      frm4 <- as.formula(paste(metric,"/(NOS^mod3$coefficients[2]) ~ NOS",sep=""))
#      mod4 <- lm(frm4, data=data)
#      
#      frm5 <- as.formula(paste(metric,"/(NOS*mod1$coefficients[2]) ~ NOS",sep=""))
#      mod5 <- lm(frm5, data=data)
#      
 
     
     plot(x=data$NOS , y= data[[metric]], main =" ",ylab = metric, xlab = "NOS",cex.lab=lab)
     #text(x=1, y=10, pos=2, paste("r = ",corr1,sep=""))
     legend("topleft",legend = c(paste("r =", round(corr1, 2))))
     #axis(cex.axis=1.2)
     abline(mod1)
     
     plot(x=data$NOS , y= data[[metric]]/data$NOS, main = " ", ylab = paste(metric,"/NOS",sep=""),xlab = "NOS",cex.lab=lab)
     # axis(cex.axis=1.2)
     legend("topleft",legend = c(paste("r =", round(corr2, 2))))
     abline(mod2)
     
#      plot(x=data$NOS, y=data[[metric]]/(data$NOS^mod3$coefficients[2]), main = " ", ylab = paste(metric,"/NOS^beta",sep=""),xlab = "NOS",cex.lab=lab)
#      abline(mod4)
#      
#      plot(x=data$NOS, y=data[[metric]]/(data$NOS*mod1$coefficients[2]), main = " ", ylab = paste(metric,"/NOS*beta",sep=""),xlab = "NOS",cex.lab=lab)
#      abline(mod5)

     
     n= n-1
      if(n<1){
        break
      }
  }
  break
}
print("done!")
#dev.off()
